# CHM Bioland project


## Local development setup

Please follow-up all the steps in this section to properly setup your local environment.

### Pre-requisites

1. You will need SSH access to the Bioland server to get DB and files. Test using `ssh -p 2974 php@www.chm-cbd.net`
2. Install latest Docker version - 18.06.0-ce-mac70 (26399)
3. Install DDEV: `brew install ddev`
4. Install GIT: `brew install git`

Note on SSH: It is important to use a SSH key without password.

### macOS development environment

This section describes the step for macOS Sierra (10.12.6).

#### 1. Clone this repository

```
git clone git@gitlab.com:bioland/website.git bioland
```

#### 2. Start the project containers

```
cd bioland
# Recreate settings.ddev.php
ddev config --docroot docroot --projectname bioland --projecttype drupal8
ddev start
```

Note: this command create two MySQL databases: 'db' and 'demo' for the two main sites. Additional can be created from `.ddev/config.yaml`


#### 3. Setup ssh remote access to Bioland test/prod servers

Copy your SSH PRIVATE KEY to `etc/ssh_key_prod` to allow connection to prod server. The public pair must be configured remotely for access. For example:

```
cp ~/.ssh/id_rsa etc/ssh_key_prod
```

#### 4. Get the production database & files

```
ddev exec drush -v sql:sync @www.prod @www
ddev exec drush -v rsync @www.prod:%files @www:%files
ddev exec drush cr
```

Now open http://bio-www.ddev.local in your browser and it should open the local installation.



### Configuring the DEMO instance (multisite)

Installation steps above cloned only WWW instance. DDEV does not natively supports multisite, so we manually configure the next one: DEMO.


#### 1. Copy settings.ddev.php and configure demo database

```
cp docroot/sites/default/settings.ddev.php docroot/sites/demo/
sed -i ''  "s/'database' => \"db\",/'database' => \"demo\",/" docroot/sites/demo/settings.ddev.php
```


#### 2. Get the production database & files

```
ddev exec drush -v sql:sync @demo.prod @demo
ddev exec drush -v rsync @demo.prod:%files @demo:%files
ddev exec drush @demo cr
```

Now open http://bio-demo.ddev.local in your browser and it should work.



### FAQ


#### 1. How do I log in into the instance?

There are two ways using drush:

1. Generate one-time login link then click the link in console: `ddev exec drush @demo uli john@doe.com` 
2. Set password to 'letmein' for any user: `ddev exec drush @www upwd john@doe.com letmein`


#### 2. Drupal complains Solr server is not reachable

For the moment, open any site's `settings.ddev.php` file and append the following line to override local solr hostname:

```
$config['search_api.server.apache_solr_server']['backend_config']['connector_config']['host'] = 'solr';
```

#### 3. Homepage shows error "The website encountered an unexpected error. Please try again later"

This problem seem to be related to a caching bug into one of the Drupal contrib modules and is temporary. Try accessing the /user URL first then back the home page. 

You can inspect the error using `docker logs ddev-bioland-web`, which shows an error like this:

```
==> /var/log/php-fpm.log <==
[04-Sep-2018 15:01:44] WARNING: [pool www] child 363 said into stderr: "NOTICE: PHP message: Uncaught PHP Exception Drupal\Component\Plugin\Exception\ContextException: "Assigned contexts were not satisfied: entity" at /var/www/html/docroot/core/lib/Drupal/Core/Plugin/Context/ContextHandler.php line 117"

==> /var/log/nginx/error.log <==
2018/09/04 15:02:48 [error] 360#360: *339 FastCGI sent in stderr: "PHP message: Uncaught PHP Exception Drupal\Component\Plugin\Exception\ContextException: "Assigned contexts were not satisfied: entity" at /var/www/html/docroot/core/lib/Drupal/Core/Plugin/Context/ContextHandler.php line 117" while reading response header from upstream, client: 172.18.0.6, server: _, request: "GET / HTTP/1.1", upstream: "fastcgi://unix:/run/php-fpm.sock:", host: "bio-www.ddev.local"
```
