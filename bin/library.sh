#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
RESET='\033[0m'

function dropDatabase() {
    echo -e "${GREEN}Dropping all tables in database...${RESET}"
    drush $1 sql-drop -y
    if [ $? -ne 0 ]; then
        echo -e "${RED}Failed to drop the database $1, aborting ...${RESET}\n";
        exit -1
    fi
}

function sqlSync() {
    src="@prod"
    if [ ! -z $1 ]; then
        src="$1.prod"
    fi
    echo -e "${GREEN}Getting ${src} environment database...${RESET}"
    drush $1 sql-sync ${src} @self -y
    if [ $? -ne 0 ]; then
        echo -e "${RED}Failed to import the 'prod' database, aborting ...${RESET}\n";
    exit -1
    fi
}

function importConfig() {
    echo -e "${GREEN}Importing default configuration...${RESET}"
    drush $1 csim -y
    if [ $? -ne 0 ]; then
        echo -e "${RED}Failed to import the default configuration, aborting ...${RESET}\n";
        exit -1
    fi
}

function updateDb() {
    echo -e "${GREEN}Running database pending updates...${RESET}"
    drush $1 updatedb -y
}

function updateEntities() {
    echo -e "${GREEN}Updating entities...${RESET}"
    drush $1 entup -y
}


function updateInstance() {
    instance=$1
    if [ ! -z "$1" ]; then
        echo -e "${GREEN}>>>> Deploying instance '$1' ...${RESET}"
    else
        echo -e "${GREEN}>>>> Deploying instance 'default' ...${RESET}"
    fi
    dropDatabase $1;
    sqlSync $1;
    importConfig ${instance};
    updateDb ${instance};
    updateEntities ${instance};
    drush $1 cr -y
}