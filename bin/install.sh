#!/bin/bash

# How to use it:
# Reinstall all sites using production database: ./install.sh
# Reinstall all sites using test database: ./install test
# Reinstall 'demo' site using production database: ./install prod demo

RED='\033[0;31m'
GREEN='\033[0;32m'
WHITE='\033[1;37m'

# Get the full path to the directory containing this script.
SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
cd "$SCRIPT_DIR"

cd "$SCRIPT_DIR/docroot"

env="prod"
if [ ! -z "$1" ]; then
  env=$1
fi

site="@sites"
if [ ! -z "$2" ]; then
  if [ $2 == "default" ] || [ $2 == 'www' ]; then
    unset site
  else
    site="@$2.local"
  fi
fi

echo -e "${GREEN}Dropping all tables in database...${WHITE}"
drush $site sql-drop -y
if [ $? -ne 0 ]; then
  echo -e "${RED}Failed to drop the database, aborting ...${WHITE}\n";
  exit -1
fi

echo -e "${GREEN}Getting '$env' environment database...${WHITE}"
drush $site sql-sync "@self.$env" @self -y
if [ $? -ne 0 ]; then
  echo -e "${RED}Failed to import the $env database, aborting ...${WHITE}\n";
  exit -1
fi

echo -e "${GREEN}Enable maintenance mode...${WHITE}"
drush $site sset system.maintenance_mode 1 -y

echo -e "${GREEN}Importing default configuration...${WHITE}"
drush $site csim -y
if [ $? -ne 0 ]; then
  echo -e "${RED}Failed to import the default configuration, aborting ...${WHITE}\n";
  exit -1
fi

echo -e "${GREEN}Running database pending updates...${WHITE}"
drush $site updatedb -y

echo -e "${GREEN}Updating entities...${WHITE}"
drush $site entup -y

echo -e "${GREEN}Resetting admin password...${WHITE}"
drush $site user-password --password="password" 1 -y

drush $site cr -y

echo -e "${GREEN}Disable maintenance mode...${WHITE}"
drush $site sset system.maintenance_mode 0 -y
