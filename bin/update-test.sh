#!/bin/bash

# This scripts executes the update of an multi-site instance
# It was designed to update the instances with new configuration
# Can be safely run on local, test or production (after a backup)

RED='\033[0;31m'
GREEN='\033[0;32m'
WHITE='\e[0m'

# Get the full path to the directory containing this script.
SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
cd "$SCRIPT_DIR/docroot"

echo -e "${GREEN}Enable maintenance mode ...${WHITE}"
drush sset system.maintenance_mode 1 -y
drush @demo sset system.maintenance_mode 1 -y

echo -e "${GREEN}Apply pre-update fix for release 1.0.6 ...${WHITE}"
drush ev 'if(!empty(\Drupal::entityManager()->getStorage("entity_queue")->load("latest_updates"))) \Drupal::entityManager()->getStorage("entity_queue")->load("latest_updates")->delete();' -y
drush @demo ev 'if(!empty(\Drupal::entityManager()->getStorage("entity_queue")->load("latest_updates"))) \Drupal::entityManager()->getStorage("entity_queue")->load("latest_updates")->delete();' -y

echo -e "${GREEN}Importing default configuration...${WHITE}"
drush csim -y
if [ $? -ne 0 ]; then
  echo -e "${RED}Failed to import the configuration, aborting (sites left maintenance mode!) ...${WHITE}\n";
  exit -1
fi
drush @demo csim -y
if [ $? -ne 0 ]; then
  echo -e "${RED}Failed to import the configuration, aborting (sites left maintenance mode!) ...${WHITE}\n";
  exit -1
fi

echo -e "${GREEN}Running database updates ...${WHITE}"
drush updatedb -y
drush @demo updatedb -y

echo -e "${GREEN}Updating entities...${WHITE}"
drush entup -y
drush @demo entup -y

drush cr -y
drush @demo cr -y

echo -e "${GREEN}Disable maintenance mode...${WHITE}"
drush sset system.maintenance_mode 0 -y
drush @demo sset system.maintenance_mode 0 -y
