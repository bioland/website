#!/bin/bash

# How to use it:
# Reinstall test sites using production database: ./install-test.sh

. library.sh

# Get the full path to the directory containing this script.
SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
cd "$SCRIPT_DIR/docroot"

declare -a ALIASES

ALIASES=( "" "@demo" "@biodivcanada" )

for alias in "${ALIASES[@]}"; do
    updateInstance "${alias}";
done
