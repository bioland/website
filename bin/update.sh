#!/bin/bash

# This scripts executes the update of an multi-site instance
# It was designed to update the instances with new configuration
# Can be safely run on local, test or production (after a backup)

RED='\033[0;31m'
GREEN='\033[0;32m'
WHITE='\e[0m'

# Get the full path to the directory containing this script.
SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
cd "$SCRIPT_DIR/docroot"

site="@sites"
if [ ! -z "$2" ]; then
  if [ $2 == "default" ] || [ $2 == 'www' ]; then
    unset site
  else
    site="@$2.local"
  fi
fi

echo -e "${GREEN}Enable maintenance mode ...${WHITE}"
drush ${site} sset system.maintenance_mode 1 -y

echo -e "${GREEN}Importing default configuration...${WHITE}"
drush ${site} csim -y
if [ $? -ne 0 ]; then
  echo -e "${RED}Failed to import the configuration, aborting (sites left maintenance mode!) ...${WHITE}\n";
  exit -1
fi

echo -e "${GREEN}Running database updates ...${WHITE}"
drush ${site} updatedb -y

echo -e "${GREEN}Updating entities...${WHITE}"
drush ${site} entup -y

echo -e "${GREEN}Updating blocks content...${WHITE}"
drush ${site} ib full -y

drush ${site} cr -y

echo -e "${GREEN}Disable maintenance mode...${WHITE}"
drush ${site} sset system.maintenance_mode 0 -y
