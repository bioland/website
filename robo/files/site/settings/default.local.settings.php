<?php

// @codingStandardsIgnoreFile

/**
 * @file
 * Local development override configuration feature.
 */

/* SECTION 1 REQUIRED OVERRIDE FOR ANY ENV */

/**
 * Database configuration.
 */
$databases = array(
  'default' =>
    array(
      'default' =>
        array(
          'database' => 'bioland_prod_xxdemoxx',
          'username' => 'USERNAME',
          'password' => 'PASSWORD',
          'host' => 'localhost',
          'port' => '3306',
          'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
          'driver' => 'mysql',
          'prefix' => '',
        ),
    ),
);

$settings['environment'] = 'prod';
$dir = dirname(DRUPAL_ROOT);

/**
 * Temporary file path:
 *
 * A local file system path where temporary files will be stored. This
 * directory should not be accessible over the web.
 *
 * Note: Caches need to be cleared when this value is changed.
 *
 * See https://www.drupal.org/node/1928898 for more information
 * about global configuration override.
 */
$config['system.file']['path']['temporary'] = '/tmp';
$config['config_split.config_split.dev']['status'] = FALSE;
$config['config_split.config_split.test']['status'] = FALSE;
$config['config_split.config_split.prod']['status'] = TRUE;

/**
 * Private file path.
 */
$settings['file_private_path'] = $dir . '/files-private/demo';

/**
 * Trusted host configuration.
 *
 * See full description in default.settings.php.
 */
$settings['trusted_host_patterns'] = array(
  'test-xdexmox.chm-cbd.net',
);

$config['swiftmailer.transport']['smtp_credentials']['swiftmailer']['password'] = 'PASSWORD';

#$config['migrate_plus.migration.inbox']['source']['urls'] = 'https://test-www.chm-cbd.net/ws-provider/inbox/889';
#$config['migrate_plus.migration.common_countries']['source']['urls'] = 'https://test-www.chm-cbd.net//ws-provider/taxonomy/countries';
#$config['migrate_plus.migration.common_treaties']['source']['urls'] = 'https://test-www.chm-cbd.net//ws-provider/taxonomy/treaties';
#$config['migrate_plus.migration.common_aichi_biodiversity_targets']['source']['urls'] = 'https://test-www.chm-cbd.net//ws-provider/taxonomy/aichi_biodiversity_targets';
#$config['migrate_plus.migration.common_planning_item_type']['source']['urls'] = 'https://test-www.chm-cbd.net//ws-provider/taxonomy/planning_item_type';
#$config['migrate_plus.migration.common_cbd_country_group']['source']['urls'] = 'https://test-www.chm-cbd.net//ws-provider/taxonomy/cbd_country_group';
#$config['migrate_plus.migration.common_data_source']['source']['urls'] = 'https://test-www.chm-cbd.net//ws-provider/taxonomy/data_source';
#$config['migrate_plus.migration.common_document_types']['source']['urls'] = 'https://test-www.chm-cbd.net//ws-provider/taxonomy/document_types';
#$config['migrate_plus.migration.common_ecosystem_types']['source']['urls'] = 'https://test-www.chm-cbd.net//ws-provider/taxonomy/ecosystem_types';
#$config['migrate_plus.migration.common_eu_targets']['source']['urls'] = 'https://test-www.chm-cbd.net//ws-provider/taxonomy/eu_targets';
#$config['migrate_plus.migration.common_event_statuses']['source']['urls'] = 'https://test-www.chm-cbd.net//ws-provider/taxonomy/event_statuses';
#$config['migrate_plus.migration.common_organization_groups']['source']['urls'] = 'https://test-www.chm-cbd.net//ws-provider/taxonomy/organization_groups';
#$config['migrate_plus.migration.common_organization_types']['source']['urls'] = 'https://test-www.chm-cbd.net//ws-provider/taxonomy/organization_types';
#$config['migrate_plus.migration.common_subjects']['source']['urls'] = 'https://test-www.chm-cbd.net//ws-provider/taxonomy/subjects';
#$config['migrate_plus.migration.common_sdg']['source']['urls'] = 'https://test-www.chm-cbd.net//ws-provider/taxonomy/sdg';
#$config['migrate_plus.migration.common_un_regions']['source']['urls'] = 'https://test-www.chm-cbd.net//ws-provider/taxonomy/un_regions';
