<?php
// @codingStandardsIgnoreFile

$aliases["self.test"] = [
  'root' => '/var/www/html/bioland/test/docroot',
  'remote-host' => 'www.chm-cbd.net',
  'remote-user' => 'php',
  'ssh-options' => '-p 2974',
  'uri' => basename(__DIR__),
];

$aliases["self.prod"] = [
  'root' => '/var/www/html/bioland/prod/docroot',
  'remote-host' => 'www.chm-cbd.net',
  'remote-user' => 'php',
  'ssh-options' => '-p 2974',
  'uri' => basename(__DIR__),
];

// Add your local aliases.
if (file_exists(dirname(__FILE__) . '/local.aliases.drushrc.php')) {
  include dirname(__FILE__) . '/local.aliases.drushrc.php';
}
