# Creating a new site using robo

## 1. Create the site directory
### 1.1. site:create (on local environment)

This command will:

* create `docroot/sites/siteName` directory
* customize all settings files within `docroot/sites/siteName` directory
* add a new config split: `config/default/config_split.config_split.siteName.yml`
* create a customized config directory `config/sites/siteName`
* add site alias to `docroot/sites/sites.php` and `drush/site-aliases/sites.php`

After creating a site using this command, commit the new files to repo.

Usage:
```
./robo site:create siteName siteCode countryCode languages
```

Parameters:
* siteName
* siteCode (default = $siteName)
* countryCode (default = FJI)
* languages (default = en,ar,es,fr,ru,zh-hans)

Example:
```
./robo site:create belgium be BEL en,fr
```

### 1.2. Add the new instance to `docroot/drush/site-aliases/aliases.drushrc.php`

### 1.3. Adding extra languages other than en,ar,es,fr,ru,zh-hans

* install the instance on local environment
* go to bio-xx.local/admin/config/regional/language
* add the extra languages
* export the configuration
```
cd docroot
drush @siteName csex -y
```

### 1.4. Git commit

## 2. site:install

This command installs a new site. If a source site alias is provided, the database will be cloned from that site.

Usage:
```
./robo site:install siteName siteCode countryCode languages
```

Parameters:
* siteName
* siteCode (default = $siteName)
* countryCode (default = FJI)
* languages (default = en,ar,es,fr,ru,zh-hans)
* sourceSiteAlias (default = NULL)

Example:
```
./robo site:install belgium be BEL en,fr demo.training
```