<?php

// @codingStandardsIgnoreFile

use Robo\Result;
use Robo\ResultData;

/**
 * This is project's console commands configuration for Robo task runner.
 *
 * @see http://robo.li/
 */
class RoboFile extends \Robo\Tasks
{

  protected $country;
  protected $name_field = 'official_name_en';
  protected $iso_2_field = 'ISO3166-1-Alpha-2';
  protected $iso_3_field = 'ISO3166-1-Alpha-3';

  protected $siteName;
  protected $siteCode;
  protected $countryIso2;
  protected $countryIso3;
  protected $countryName;
  protected $languages;

  protected $configDir;
  protected $configSplitFile;

  protected $demoSiteName = 'xxdemoxx';
  protected $demoSiteCode = 'xdexmox';
  protected $demoSiteTitle = 'xxdemotitlexx';
  protected $demoCountryCode = 'xxxCOUNTRYCODExxx';

  protected $defaultLanguages = ['en', 'ar', 'es', 'fr', 'ru', 'zh-hans'];

  protected function prepare($siteName, $siteCode = '', $countryIso3 = 'FJI', $languages = 'en,ar,es,fr,ru,zh-hans') {
    $countries = $this->getCountries();
    if (empty($countries[$countryIso3])) {
      return new Result($this->task(), ResultData::EXITCODE_ERROR, "Invalid country code: {$countryIso3}");
    }
    $this->country = $countries[$countryIso3];

    $this->siteName = $siteName;
    $this->siteCode = empty($siteCode) ? $siteName : $siteCode;
    $this->countryIso2 = $this->country[$this->iso_2_field];
    $this->countryIso3 = $countryIso3;
    $this->countryName = $this->country[$this->name_field];
    $this->languages = explode(',', $languages);
    foreach ($this->languages as $language) {
      if (!in_array($language, $this->defaultLanguages)) {
        return new Result($this->task(), ResultData::EXITCODE_ERROR, "Invalid language: {$language}");
      }
    }
    if (!in_array('en', $this->languages)) {
      $this->languages[] = 'en';
    }
    $this->siteDir = "../docroot/sites/{$this->siteName}";
    $this->configDir = "../config/sites/{$this->siteName}";
    $this->configSplitFile = "../config/default/config_split.config_split.{$this->siteName}.yml";
  }

  protected function replaceString($search, $replacement, $path) {
    // Replace single quote with \x27s.
    $replacement = str_replace('\'', '\\x27', $replacement);
    $this->_exec("find {$path} -type f -exec sed -i 's/{$search}/{$replacement}/g' {} +");
  }

  protected function createSiteDirectory() {
    $this->_copyDir("files/site", $this->siteDir);
    $this->replaceString($this->demoSiteName, $this->siteName, $this->siteDir);
    $this->replaceString($this->demoSiteCode, $this->siteCode, $this->siteDir);
    $this->replaceString($this->demoSiteTitle, $this->countryName, $this->siteDir);
    $this->replaceString($this->demoCountryCode, $this->countryIso3, "{$this->siteDir}/settings.php");
  }

  protected function createSiteConfigurationSplit() {
    $this->_copyDir("files/config", $this->configDir);
    $this->_copy("files/config_split.config_split.{$this->demoSiteName}.yml", $this->configSplitFile);
    $this->replaceString($this->demoSiteName, $this->siteName, $this->configSplitFile);
    $this->replaceString($this->demoSiteName, $this->siteName, $this->configDir);
    $this->replaceString($this->demoSiteCode, $this->siteCode, $this->configSplitFile);
    $this->replaceString($this->demoSiteTitle, $this->countryName, $this->configDir);
  }

  protected function removeExtraLanguages() {
    foreach ($this->defaultLanguages as $language) {
      if (!in_array($language, $this->languages)) {
        $this->_remove("{$this->configDir}/language/{$language}");
        $this->_remove("{$this->configDir}/language.entity.{$language}.yml");
      }
    }
  }

  protected function addSiteAliases() {
    $localSiteString = '$sites[\'bio-' . $this->siteCode . '.local\'] = \'' . $this->siteName . '\';';
    $testSiteString = '$sites[\'test-' . $this->siteCode . '.chm-cbd.net\'] = \'' . $this->siteName . '\';';
    $this->taskWriteToFile('../docroot/sites/sites.php')
      ->append(TRUE)
      ->line($localSiteString)
      ->line($testSiteString)
      ->run();

    $extraAlias = '$sites[\'' . $this->siteName . '\'] = \'' . $this->siteName . '\';';
    $this->taskWriteToFile('../drush/site-aliases/sites.php')
      ->append(TRUE)
      ->line($extraAlias)
      ->run();
  }

  /**
   * @command site:create
   */
  public function createSite($siteName, $siteCode = '', $countryIso3 = 'FJI', $languages = 'en,ar,es,fr,ru,zh-hans') {
    $this->prepare($siteName, $siteCode, $countryIso3, $languages);
    $this->createSiteDirectory();
    $this->createSiteConfigurationSplit();
    $this->removeExtraLanguages();
    $this->addSiteAliases();
  }

  /**
   * @command site:install
   */
  public function installSite($siteName, $siteCode = '', $countryIso3 = 'FJI', $languages = 'en,ar,es,fr,ru,zh-hans', $sourceSiteAlias = NULL) {
    $this->prepare($siteName, $siteCode, $countryIso3, $languages);
    $drushCommand = "cd ../docroot && drush @{$this->siteName}";
    $execStack = $this->taskExecStack()->stopOnFail();
    $execStack->exec("cp -R files/files {$this->siteDir}");
    $execStack->exec("cp files/flags/" . strtolower($this->countryIso2) . ".png {$this->siteDir}/files/logo.png");
    $execStack->exec("mkdir -p {$this->siteDir}/files/translations");
    if (empty($sourceSiteAlias)) {
      $execStack->exec("{$drushCommand} site-install --verbose config_installer -y");
      if ($execStack->run()->wasSuccessful()) {
        return $this->postInstallSite($siteName);
      }
    }
    else {
      $execStack->exec("{$drushCommand} sql-sync @{$sourceSiteAlias} @self -y");
      if ($execStack->run()->wasSuccessful() && $this->updateSite($siteName)->wasSuccessful()) {
        $this->_exec("{$drushCommand} rsync @{$sourceSiteAlias}:%files @self:%files");
        return $this->runMigrations($siteName);
      }
    }
    return FALSE;
  }

  /**
   * @command site:update
   */
  public function updateSite($siteName) {
    $this->prepare($siteName);
    $drushCommand = "cd ../docroot && drush @{$this->siteName}";
    $execStack =$this->taskExecStack()
      ->stopOnFail()
      ->exec("{$drushCommand} cr")
      ->exec("{$drushCommand} csim -y")
      ->exec("{$drushCommand} csim -y") // Running csim only one time doesn't do the job. Don't know why.
      ->exec("{$drushCommand} updatedb -y")
      ->exec("{$drushCommand} entup -y")
      ->exec("{$drushCommand} locale-check")
      ->exec("{$drushCommand} locale-update")
      ->exec("{$drushCommand} sapi-c")
      ->exec("{$drushCommand} sapi-i");
    return $execStack->run();
  }


  /**
   * @command site:updateClonedSite
   */
  public function updateClonedSite($siteName) {
    $this->prepare($siteName);
    $drushCommand = "cd ../docroot && drush @{$this->siteName}";
    $result = $this->taskExecStack()
      ->stopOnFail()
      ->exec("{$drushCommand} en profile_switcher -y")
      ->exec("{$drushCommand} switch-profile config_installer -y")
      ->run();
    if ($result->wasSuccessful()) {
      return $this->updateSite($siteName);
    }
    return FALSE;
  }

  /**
   * @command site:postInstall
   */
  public function postInstallSite($siteName) {
    $this->prepare($siteName);
    $drushCommand = "cd ../docroot && drush @{$this->siteName}";
    if ($this->updateSite($siteName)->wasSuccessful()) {
      $this->taskExecStack()
        ->exec("{$drushCommand} uuinfo 1 --mail=drupal@eaudeweb.ro --roles=administrator")
        ->exec("{$drushCommand} import-default-content")
        ->run();
      $this->runMigrations($siteName);
    }
    return FALSE;
  }

  /**
   * @command site:runMigrations
   */
  public function runMigrations($siteName) {
    $this->prepare($siteName);
    $drushCommand = "cd ../docroot && drush @{$this->siteName}";
    return $this->taskExecStack()
      ->exec("{$drushCommand} migrate-import common_countries")
      ->exec("{$drushCommand} migrate-rollback cbd_documents")
      ->exec("{$drushCommand} migrate-import --all")
      ->run();
  }

  protected function getCountries() {
    // https://github.com/datasets/country-codes/blob/master/data/country-codes.csv
    $countries = [];
    if (($handle = fopen('files/countries.csv', 'r')) !== FALSE) {
      // Skip first line
      $header = fgetcsv($handle, 3000, ",");
      while (($data = fgetcsv($handle, 3000, ",")) !== FALSE) {
        $country = [];
        foreach ($data as $key => $datum) {
          $country[$header[$key]] = $datum;
        }
        $countries[$country[$this->iso_3_field]] = $country;
      }
    }
    return $countries;
  }

}