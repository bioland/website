<?php

namespace Drupal\entity_reports\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\entity_reports\ReportGenerator;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class EntityReportsController handle routes for custom entity reports.
 */
class EntityReportsController extends ControllerBase {

  public function exportContentTypesJson() {
    $structure = ReportGenerator::generateContentTypesReport();
    $response = new Response();
    $response->setContent(json_encode($structure));
    $response->headers->set('Content-Type', 'application/json');
    return $response;
  }

  public function exportTaxonomyStructureJson() {
    $structure = ReportGenerator::generateTaxonomyReport();
    $response = new Response();
    $response->setContent(json_encode($structure));
    $response->headers->set('Content-Type', 'application/json');
    return $response;
  }
}
