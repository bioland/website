<?php

# Load local services customizations if present
if (is_readable($app_root . '/' . $site_path . '/local.services.yml')) {
  $settings['container_yamls'][] = $app_root . '/' . $site_path . '/local.services.yml';
}

$settings['install_profile'] = 'standard';
$config_directories['sync'] = '../config/default';

$bioland_site = str_replace('sites/', '', $site_path);
if ($bioland_site == 'default') {
  $bioland_site = 'www';
}
if (!empty($settings['bioland_site'])) {
  $bioland_site = $settings['bioland_site'];
}

$config["config_split.config_split.{$bioland_site}"]['status'] = TRUE;

// Configure config splits - default to production settings
// Customize locally with $settings['environment'] = 'dev'
$environment = !empty($settings['environment']) ? strtolower($settings['environment']) : 'prod';
$config['config_split.config_split.prod']['status'] = TRUE;
$config['config_split.config_split.dev']['status'] = FALSE;
if ($environment == 'dev') {
  $config['config_split.config_split.prod']['status'] = FALSE;
  $config['config_split.config_split.dev']['status'] = TRUE;
}

// Configure unique SOLR key per each site
$config['search_api_solr.settings']['site_hash'] = '67j0rw' . $bioland_site;

// Temporary storage
$config['system.file']['path']['temporary'] = '/tmp';

// Logging setup
$config['system.logging']['error_level'] = 'none';

// Hash salt
$salt_file = realpath($app_root . '/../salt.txt');
if (file_exists($salt_file)) {
  $settings['hash_salt'] = file_get_contents($salt_file);
}

// Default migration sources for shared taxonomies and content
$config['migrate_plus.migration.common_countries']['source']['urls'] = 'https://www.chm-cbd.net/ws-provider/taxonomy/countries';
$config['migrate_plus.migration.common_treaties']['source']['urls'] = 'https://www.chm-cbd.net/ws-provider/taxonomy/treaties';
$config['migrate_plus.migration.common_aichi_biodiversity_targets']['source']['urls'] = 'https://www.chm-cbd.net/ws-provider/taxonomy/aichi_biodiversity_targets';
$config['migrate_plus.migration.common_planning_item_type']['source']['urls'] = 'https://www.chm-cbd.net/ws-provider/taxonomy/planning_item_type';
$config['migrate_plus.migration.common_cbd_country_group']['source']['urls'] = 'https://www.chm-cbd.net/ws-provider/taxonomy/cbd_country_group';
$config['migrate_plus.migration.common_data_source']['source']['urls'] = 'https://www.chm-cbd.net/ws-provider/taxonomy/data_source';
$config['migrate_plus.migration.common_document_types']['source']['urls'] = 'https://www.chm-cbd.net/ws-provider/taxonomy/document_types';
$config['migrate_plus.migration.common_ecosystem_types']['source']['urls'] = 'https://www.chm-cbd.net/ws-provider/taxonomy/ecosystem_types';
$config['migrate_plus.migration.common_eu_targets']['source']['urls'] = 'https://www.chm-cbd.net/ws-provider/taxonomy/eu_targets';
$config['migrate_plus.migration.common_event_statuses']['source']['urls'] = 'https://www.chm-cbd.net/ws-provider/taxonomy/event_statuses';
$config['migrate_plus.migration.common_organization_groups']['source']['urls'] = 'https://www.chm-cbd.net/ws-provider/taxonomy/organization_groups';
$config['migrate_plus.migration.common_organization_types']['source']['urls'] = 'https://www.chm-cbd.net//ws-provider/taxonomy/organization_types';
$config['migrate_plus.migration.common_subjects']['source']['urls'] = 'https://www.chm-cbd.net//ws-provider/taxonomy/subjects';
$config['migrate_plus.migration.common_sdg']['source']['urls'] = 'https://www.chm-cbd.net//ws-provider/taxonomy/sdg';
$config['migrate_plus.migration.common_un_regions']['source']['urls'] = 'https://www.chm-cbd.net//ws-provider/taxonomy/un_regions';

# Allow customizing global settings (i.e. to configure global secrets).
if (file_exists($app_root . '/sites/settings.common.local.php')) {
  include $app_root . '/sites/settings.common.local.php';
}
