<?php

/**
 * @file
 * Configure mapping between websites (domains) and corresponding disk dirs.
 */

// Local development.
$sites['bio-demo.ddev.local'] = 'demo';
$sites['bio-demo.local'] = 'demo';
$sites['bio-biodivcanada.local'] = 'biodivcanada';
$sites['bio-vanuatu.local'] = 'vanuatu';
$sites['bio-eg.local'] = 'egypt';
$sites['bio-demo.ddev.test'] = 'demo';
$sites['bio-demo.test'] = 'demo';
$sites['bio-biodivcanada.test'] = 'biodivcanada';
$sites['bio-vanuatu.test'] = 'vanuatu';
$sites['bio-eg.test'] = 'egypt';

// Test instance.
$sites['test-demo.chm-cbd.net'] = 'demo';
$sites['test-biodivcanada.chm-cbd.net'] = 'biodivcanada';
$sites['test-eg.chm-cbd.net'] = 'egypt';

// Production instance.
$sites['demo.chm-cbd.net'] = 'demo';
$sites['biodivcanada.chm-cbd.net'] = 'biodivcanada';
$sites['www.biodivcanada.ca'] = 'biodivcanada';
$sites['new-vu.chm-cbd.net'] = 'vanuatu';

// Add your local sites.
if (file_exists(dirname(__FILE__) . '/local.sites.php')) {
  include dirname(__FILE__) . '/local.sites.php';
}
