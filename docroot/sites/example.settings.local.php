<?php

// @codingStandardsIgnoreFile

/**
 * @file
 * Local development override configuration feature.
 *
 * To activate this feature, copy and rename it such that its path plus
 * filename is 'sites/default/settings.local.php'. Then, go to the bottom of
 * 'sites/default/settings.php' and uncomment the commented lines that mention
 * 'settings.local.php'.
 *
 * If you are using a site name in the path, such as 'sites/example.com', copy
 * this file to 'sites/example.com/settings.local.php', and uncomment the lines
 * at the bottom of 'sites/example.com/settings.php'.
 */

// Database configuration.
$databases = array(
  'default' =>
    array(
      'default' =>
        array(
          'database' => 'bio_www',
          'username' => 'root',
          'password' => 'root',
          'host' => 'localhost',
          'port' => '3306',
          'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
          'driver' => 'mysql',
          'prefix' => '',
        ),
    ),
);

$settings['environment'] = 'dev';

// Private file storage.
$settings['file_private_path'] = '/path/to/repo/files-private/www';

// Add here your domain.
$settings['trusted_host_patterns'] = array(
  'bio-www.local',
);

// Development mode
$config['system.logging']['error_level'] = 'all';
$config['system.performance']['cache']['page']['max_age'] = 0;
$config['system.performance']['css']['preprocess'] = FALSE;
$config['system.performance']['js']['preprocess'] = FALSE;
$settings['container_yamls'][] = DRUPAL_ROOT . '/sites/development.services.yml';
$settings['cache']['bins']['render'] = 'cache.backend.null';
$settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.null';
$settings['cache']['bins']['page'] = 'cache.backend.null';

// Site's own hash-salt
// $settings['hash_salt'] = 'dummy-hash-salt-to-avoid-breaking-your-site';

// Configure SMTP password for swiftmailer
// $config['swiftmailer.transport']['smtp_credentials']['swiftmailer']['password'] = '';

// Configure ReCaptcha keys
// $config['recaptcha.settings']['site_key'] = '';
// $config['recaptcha.settings']['secret_key'] = '';

// Override migration sources for shared taxonomies and content
//$config['migrate_plus.migration.common_countries']['source']['urls'] = 'https://www.chm-cbd.net/ws-provider/taxonomy/countries';
//$config['migrate_plus.migration.common_treaties']['source']['urls'] = 'https://www.chm-cbd.net/ws-provider/taxonomy/treaties';
//$config['migrate_plus.migration.common_aichi_biodiversity_targets']['source']['urls'] = 'https://www.chm-cbd.net/ws-provider/taxonomy/aichi_biodiversity_targets';
//$config['migrate_plus.migration.common_planning_item_type']['source']['urls'] = 'https://www.chm-cbd.net/ws-provider/taxonomy/planning_item_type';
//$config['migrate_plus.migration.common_cbd_country_group']['source']['urls'] = 'https://www.chm-cbd.net/ws-provider/taxonomy/cbd_country_group';
//$config['migrate_plus.migration.common_data_source']['source']['urls'] = 'https://www.chm-cbd.net/ws-provider/taxonomy/data_source';
//$config['migrate_plus.migration.common_document_types']['source']['urls'] = 'https://www.chm-cbd.net/ws-provider/taxonomy/document_types';
//$config['migrate_plus.migration.common_ecosystem_types']['source']['urls'] = 'https://www.chm-cbd.net/ws-provider/taxonomy/ecosystem_types';
//$config['migrate_plus.migration.common_eu_targets']['source']['urls'] = 'https://www.chm-cbd.net/ws-provider/taxonomy/eu_targets';
//$config['migrate_plus.migration.common_event_statuses']['source']['urls'] = 'https://www.chm-cbd.net/ws-provider/taxonomy/event_statuses';
//$config['migrate_plus.migration.common_organization_groups']['source']['urls'] = 'https://www.chm-cbd.net/ws-provider/taxonomy/organization_groups';
//$config['migrate_plus.migration.common_organization_types']['source']['urls'] = 'https://www.chm-cbd.net//ws-provider/taxonomy/organization_types';
//$config['migrate_plus.migration.common_subjects']['source']['urls'] = 'https://www.chm-cbd.net//ws-provider/taxonomy/subjects';
//$config['migrate_plus.migration.common_sdg']['source']['urls'] = 'https://www.chm-cbd.net//ws-provider/taxonomy/sdg';
//$config['migrate_plus.migration.common_un_regions']['source']['urls'] = 'https://www.chm-cbd.net//ws-provider/taxonomy/un_regions';
